<?php

if (!function_exists('write_log')) {

    function write_log($log) {
        if (true === WP_DEBUG) {
            if (is_array($log) || is_object($log)) {
                error_log(print_r($log, true));
            } else {
                error_log($log);
            }
        }
    }

}

// login to Shiphero API
function sh_login(){
    $curl = curl_init();

    $current_options = get_option( 'shiphero_option_name' );
    $sh_email = $current_options['shiphero_email'];
    $sh_pass = $current_options['shiphero_pass'];
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://public-api.shiphero.com/auth/token',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS =>'{ 
      "username": "'.$sh_email.'", 
      "password": "'.$sh_pass.'"
    }',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    write_log($response);
    $response = json_decode($response, true);
    return $response['access_token'];
}

function check_product($sku, $token){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://public-api.shiphero.com/graphql',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_POSTFIELDS =>'{"query":"query {\\r\\n  product(sku:\\"'.$sku.'\\") {\\r\\n    request_id\\r\\n    complexity\\r\\n    data {\\r\\n      id\\r\\n      legacy_id\\r\\n      account_id\\r\\n      name\\r\\n      sku\\r\\n      price\\r\\n      value\\r\\n      dimensions {\\r\\n        weight\\r\\n        height\\r\\n        width\\r\\n        length\\r\\n      }\\r\\n      warehouse_products {\\r\\n        warehouse_id\\r\\n        warehouse_identifier\\r\\n        on_hand\\r\\n      }\\r\\n      images {\\r\\n        src\\r\\n        position\\r\\n      }\\r\\n    }\\r\\n  }\\r\\n}","variables":{}}',
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Authorization: Bearer ' . $token
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    write_log($response);
    $response = json_decode($response, true);
    return $response;
}