<?php

class ShipheroSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'Shiphero Settings', 
            'manage_options', 
            'shiphero-setting-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'shiphero_option_name' );
        ?>
        <div class="wrap">
            <h1>Shiphero Settings</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'shiphero_option_group' );
                do_settings_sections( 'shiphero-setting-admin' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'shiphero_option_group', // Option group
            'shiphero_option_name', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'shiphero_setting_section_id', // ID
            'Shiphero Credentials', // Title
            array( $this, 'print_section_info' ), // Callback
            'shiphero-setting-admin' // Page
        );  

        add_settings_field(
            'shiphero_email', // ID
            'Shiphero Email', // Title 
            array( $this, 'shiphero_email_callback' ), // Callback
            'shiphero-setting-admin', // Page
            'shiphero_setting_section_id' // Section           
        );      

        add_settings_field(
            'shiphero_pass', 
            'Shiphero Password', 
            array( $this, 'shiphero_pass_callback' ), 
            'shiphero-setting-admin', 
            'shiphero_setting_section_id'
        );      
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['shiphero_email'] ) )
            $new_input['shiphero_email'] = sanitize_text_field( $input['shiphero_email'] );

        if( isset( $input['shiphero_pass'] ) )
            $new_input['shiphero_pass'] = sanitize_text_field( $input['shiphero_pass'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your Shiphero credentials below:';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function shiphero_email_callback()
    {
        printf(
            '<input type="text" id="shiphero_email" name="shiphero_option_name[shiphero_email]" value="%s" />',
            isset( $this->options['shiphero_email'] ) ? esc_attr( $this->options['shiphero_email']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function shiphero_pass_callback()
    {
        printf(
            '<input type="password" id="shiphero_pass" name="shiphero_option_name[shiphero_pass]" value="%s" />',
            isset( $this->options['shiphero_pass'] ) ? esc_attr( $this->options['shiphero_pass']) : ''
        );
    }


}

?>