<?php

// helper functions
include_once 'helper.php';

// Shiphero sync on NEW product or product UPDATE
function add_edit_product( $post_id, $post, $update ) {
    if ( 'product' !== $post->post_type ) {
        return;
    }

    $product = wc_get_product( $post->ID );
    $token = sh_login();

    $sku = $product->get_sku();

    $check_product = check_product($sku, $token);

    // NEW PRODUCT
    if( isset($check_product['errors']) ){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://public-api.shiphero.com/graphql',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{"query":"mutation {\\r\\n  product_create(\\r\\n    data: {\\r\\n      name: \\"'.$product->get_name().'\\"\\r\\n      sku: \\"'.$sku.'\\"\\r\\n      warehouse_products: {\\r\\n        warehouse_id: \\"V2FyZWhvdXNlOjcyMzA5\\"\\r\\n        on_hand: '.$product->get_stock_quantity().',\\r\\n        price: \\"'.$product->get_regular_price().'\\",\\r\\n        value: \\"'.$product->get_sale_price().'\\"\\r\\n      }\\r\\n      country_of_manufacture: \\"US\\"\\r\\n      dimensions: { weight: \\"'.$product->get_weight().'\\", height: \\"'.$product->get_height().'\\", width: \\"'.$product->get_width().'\\", length: \\"'.$product->get_length().'\\" }\\r\\n      kit: false\\r\\n      kit_build: false\\r\\n      no_air: false\\r\\n      final_sale: false\\r\\n      customs_value: \\"1.00\\"\\r\\n      not_owned: true\\r\\n      dropship: false\\r\\n    }\\r\\n  ) {\\r\\n    request_id\\r\\n    complexity\\r\\n    product {\\r\\n      id\\r\\n      legacy_id\\r\\n      account_id\\r\\n      name\\r\\n      sku\\r\\n      barcode\\r\\n      country_of_manufacture\\r\\n      dimensions {\\r\\n        weight\\r\\n        height\\r\\n        width\\r\\n        length\\r\\n      }\\r\\n    }\\r\\n  }\\r\\n}","variables":{}}',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        
        write_log("NEW PRODUCT" . $product->get_regular_price());
        write_log($response);
    }else{
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://public-api.shiphero.com/graphql',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{"query":"mutation {\\r\\n  product_update(\\r\\n    data: {\\r\\n      sku: \\"'.$sku.'\\"\\r\\n      name: \\"'.$product->get_name().'\\"\\r\\n      dimensions: {\\r\\n        weight: \\"'.$product->get_weight().'\\"\\r\\n        height: \\"'.$product->get_height().'\\"\\r\\n        width: \\"'.$product->get_width().'\\"\\r\\n        length: \\"'.$product->get_length().'\\"\\r\\n      }\\r\\n    }\\r\\n  ) {\\r\\n    request_id\\r\\n    complexity\\r\\n  }\\r\\n}","variables":{}}',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        write_log("UP PRODUCT");
        write_log($response);
    }
    
    
}

add_action( 'wp_insert_post', 'add_edit_product', 10, 3 );