<?php

// helper functions
include_once 'helper.php';

add_action( 'woocommerce_thankyou', 'send_to_shiphero',  1, 2  );
function send_to_shiphero( $order_id ) {
	$order = wc_get_order($order_id);
    $items = $order->get_items();
    $site_title = get_bloginfo( 'name' );

    $order_items = '';
    foreach($items as $item){
    	$item_product = $item->get_product();
    	$order_items .= '[\\r\\n      {\\r\\n        sku: \\"'.$item_product->get_sku().'\\"\\r\\n        quantity: '.$item->get_quantity().'\\r\\n        partner_line_item_id: \\"'.$item->get_product_id().'-'.time().'\\"\\r\\n        price: \\"'.$item->get_total().'\\"\\r\\n        product_name: \\"'.$item->get_name().'\\"\\r\\n        fulfillment_status: \\"pending\\"\\r\\n        quantity_pending_fulfillment: '.$item->get_quantity().'\\r\\n        warehouse_id: \\"V2FyZWhvdXNlOjcyMzA5\\"\\r\\n      },\\r\\n      ]';
    }

    // login to Shiphero API
	$token = sh_login();
	//var_dump($token);


	// create order in Shiphero
	$curl1 = curl_init();

	curl_setopt_array($curl1, array(
	  CURLOPT_URL => 'https://public-api.shiphero.com/graphql',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'POST',
	  CURLOPT_POSTFIELDS =>'{"query":"mutation {\\r\\n  order_create(\\r\\n    data: {\\r\\n      order_number: \\"'.$order->get_id().'\\"\\r\\n      shop_name: \\"'.$site_title.'\\"\\r\\n      fulfillment_status: \\"pending\\"\\r\\n      order_date: \\"'.$order->get_date_created().'\\"\\r\\n      total_tax: \\"'.$order->get_total_tax().'\\"\\r\\n      subtotal: \\"'.$order->get_subtotal().'\\"\\r\\n      total_discounts: \\"'.$order->get_total_discount().'\\"\\r\\n      total_price: \\"'.$order->get_total().'\\"\\r\\n      shipping_lines: {\\r\\n        title: \\"'.$order->get_shipping_method().'\\"\\r\\n        price: \\"'.$order->get_shipping_total().'\\"\\r\\n        carrier: \\"'.$order->get_shipping_method().'\\"\\r\\n        method: \\"'.$order->get_shipping_total().'\\"\\r\\n      }\\r\\n      shipping_address: {\\r\\n        first_name: \\"'.$order->get_shipping_first_name().'\\"\\r\\n        last_name: \\"'.$order->get_shipping_last_name().'\\"\\r\\n        company: \\"'.$order->get_shipping_company().'\\"\\r\\n        address1: \\"'.$order->get_shipping_address_1().'\\"\\r\\n        address2: \\"'.$order->get_shipping_address_2().'\\"\\r\\n        city: \\"'.$order->get_shipping_city().'\\"\\r\\n        state: \\"'.$order->get_shipping_state().'\\"\\r\\n        state_code: \\"'.$order->get_shipping_state().'\\"\\r\\n        zip: \\"'.$order->get_shipping_postcode().'\\"\\r\\n        country: \\"'.$order->get_shipping_country().'\\"\\r\\n        country_code: \\"'.$order->get_shipping_country().'\\"\\r\\n        email: \\"'.$order->get_billing_email().'\\"\\r\\n        phone: \\"'.$order->get_billing_phone().'\\"\\r\\n      }\\r\\n      billing_address: {\\r\\n        first_name: \\"'.$order->get_billing_first_name().'\\"\\r\\n        last_name: \\"'.$order->get_billing_last_name().'\\"\\r\\n        company: \\"'.$order->get_billing_company().'\\"\\r\\n        address1: \\"'.$order->get_billing_address_1().'\\"\\r\\n        address2: \\"'.$order->get_billing_address_2().'\\"\\r\\n        city: \\"'.$order->get_billing_city().'\\"\\r\\n        state: \\"'.$order->get_billing_state().'\\"\\r\\n        state_code: \\"'.$order->get_billing_state().'\\"\\r\\n        zip: \\"'.$order->get_billing_postcode().'\\"\\r\\n        country: \\"'.$order->get_billing_country().'\\"\\r\\n        country_code: \\"'.$order->get_billing_country().'\\"\\r\\n        email: \\"'.$order->get_billing_email().'\\"\\r\\n        phone: \\"'.$order->get_billing_phone().'\\"\\r\\n      }\\r\\n      line_items:'.$order_items.'\\r\\n      required_ship_date: \\"'.$order->get_date_created().'\\"\\r\\n    }\\r\\n      \\r\\n  ) {\\r\\n    request_id\\r\\n    complexity\\r\\n    order {\\r\\n      id\\r\\n      order_number\\r\\n      shop_name\\r\\n      fulfillment_status\\r\\n      order_date\\r\\n      total_tax\\r\\n      subtotal\\r\\n      total_discounts\\r\\n      total_price\\r\\n      custom_invoice_url\\r\\n      account_id\\r\\n      email\\r\\n      profile\\r\\n      packing_note\\r\\n      required_ship_date\\r\\n      shipping_address {\\r\\n        first_name\\r\\n        last_name\\r\\n        company\\r\\n        address1\\r\\n        address2\\r\\n        city\\r\\n        state\\r\\n        state_code\\r\\n        zip\\r\\n        country\\r\\n        country_code\\r\\n        email\\r\\n        phone\\r\\n      }\\r\\n      line_items(first: 2) {\\r\\n        edges {\\r\\n          node {\\r\\n            id\\r\\n            sku\\r\\n            product_id\\r\\n            quantity\\r\\n            product_name\\r\\n            fulfillment_status\\r\\n            quantity_pending_fulfillment\\r\\n            quantity_allocated\\r\\n            backorder_quantity\\r\\n            eligible_for_return\\r\\n            customs_value\\r\\n            warehouse_id\\r\\n            locked_to_warehouse_id\\r\\n          }\\r\\n        }\\r\\n      }\\r\\n    }\\r\\n  }\\r\\n}","variables":{}}',
	  CURLOPT_HTTPHEADER => array(
	    'Content-Type: application/json',
	    'Authorization: Bearer ' . $token
	  ),
	));

	$response1 = curl_exec($curl1);

	curl_close($curl1);
	write_log($response1);
	//echo $response1;
}