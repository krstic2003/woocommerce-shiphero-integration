<?php
if ( ! defined( 'WPINC' ) ) {
    die;
}
/* 
* Check if WooCommerce is active 
*/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    function shiphero_shipping_method() {
        class ShipHero_Shipping_Method extends WC_Shipping_Method {
            /** 
            * Constructor for your shipping class 
            * 
            * @access public 
            * @return void 
            */
            public function __construct() {
                $this->id                 = 'shiphero'; 
                $this->method_title       = __( 'ShipHero Shipping', 'shiphero' );  
                $this->method_description = __( 'Custom Shipping Method for ShipHero', 'shiphero' ); 
                // Availability & Countries 
                $this->availability = 'including';
                $this->countries = array(
                "AD","AE","AF","AG","AI","AL","AM","AN","AO","AQ","AR","AS","AT","AU","AW","AX","AZ","BA","BB","BD","BE","BF","BG","BH","BI","BJ","BL","BM","BN","BO","BQ","BR","BS","BT","BV","BW","BY","BZ","CA","CC","CD","CF","CG","CH","CI","CK","CL","CM","CN","CO","CR","CS","CU","CV","CW","CX","CY","CZ","DE","DJ","DK","DM","DO","DZ","EC","EE","EG","EH","ER","ES","ET","FI","FJ","FK","FM","FO","FR","GA","GB","GD","GE","GF","GG","GH","GI","GL","GM","GN","GP","GQ","GR","GS","GT","GU","GW","GY","HK","HM","HN","HR","HT","HU","ID","IE","IL","IM","IN","IO","IQ","IR","IS","IT","JE","JM","JO","JP","KE","KG","KH","KI","KM","KN","KP","KR","KW","KY","KZ","LA","LB","LC","LI","LK","LR","LS","LT","LU","LV","LY","MA","MC","MD","ME","MF","MG","MH","MK","ML","MM","MN","MO","MP","MQ","MR","MS","MT","MU","MV","MW","MX","MY","MZ","NA","NC","NE","NF","NG","NI","NL","NO","NP","NR","NU","NZ","OM","PA","PE","PF","PG","PH","PK","PL","PM","PN","PR","PS","PT","PW","PY","QA","RE","RO","RS","RU","RW","SA","SB","SC","SD","SE","SG","SH","SI","SJ","SK","SL","SM","SN","SO","SR","SS","ST","SV","SX","SY","SZ","TC","TD","TF","TG","TH","TJ","TK","TL","TM","TN","TO","TR","TT","TV","TW","TZ","UA","UG","UM","US","UY","UZ","VA","VC","VE","VG","VI","VN","VU","WF","WS","XK","YE","YT","ZA","ZM","ZW"
                );
                $this->init();
                $this->enabled = isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'yes';
                $this->title = isset( $this->settings['title'] ) ? $this->settings['title'] : __( 'ShipHero Shipping', 'shiphero' );
            }
            /** 
            * Init your settings 
            * 
            * @access public 
            * @return void 
            */
            function init() {
                // Load the settings API 
                $this->init_form_fields(); 
                $this->init_settings(); 
                // Save settings in admin if you have any defined 
                add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
            }
            /** 
            * Define settings field for this shipping 
            * @return void 
            */
            function init_form_fields() { 
                $this->form_fields = array(
                 'enabled' => array(
                      'title' => __( 'Enable', 'shiphero' ),
                      'type' => 'checkbox',
                      'description' => __( 'Enable this shipping.', 'shiphero' ),
                      'default' => 'yes'
                      ),
                 'title' => array(
                    'title' => __( 'Title', 'shiphero' ),
                      'type' => 'text',
                      'description' => __( 'Title to be display on site', 'shiphero' ),
                      'default' => __( 'ShipHero Shipping', 'shiphero' )
                      ),
                 'weight' => array(
                    'title' => __( 'Weight (kg)', 'shiphero' ),
                      'type' => 'number',
                      'description' => __( 'Maximum allowed weight', 'shiphero' ),
                      'default' => 100
                      ),
                 );
            }
            /** 
            * This function is used to calculate the shipping cost. Within this function we can check for weights, dimensions and other parameters. 
            * 
            * @access public 
            * @param mixed $package 
            * @return void 
            */
            public function calculate_shipping( $package = array() ) {
               
                $rate = array(
                    'id' => $this->id,
                    'label' => $this->title,
                    'cost' => 20.00
                );
                $this->add_rate( $rate );
               
            }
        }
    }
    add_action( 'woocommerce_shipping_init', 'shiphero_shipping_method' );

    function add_shiphero_shipping_method( $methods ) {
        $methods[] = 'ShipHero_Shipping_Method';
        return $methods;
    }
    add_filter( 'woocommerce_shipping_methods', 'add_shiphero_shipping_method' );

    function shiphero_validate_order( $posted )   {
        $packages = WC()->shipping->get_packages();
        $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
        
        if( is_array( $chosen_methods ) && in_array( 'shiphero', $chosen_methods ) ) {
            
            foreach ( $packages as $i => $package ) {
                if ( $chosen_methods[ $i ] != "shiphero" ) {
                            
                    continue;
                            
                }
                $ShipHero_Shipping_Method = new ShipHero_Shipping_Method();
                $weightLimit = (int) $ShipHero_Shipping_Method->settings['weight'];
                $weight = 0;
                foreach ( $package['contents'] as $item_id => $values ) 
                { 
                    $_product = $values['data']; 
                    $p_weight = $_product->get_weight();
                    $p_qty = $values['quantity'];
                    $weight = (float)$weight + (float)$p_weight * (float)$p_qty; 
                }
                $weight = wc_get_weight( $weight, 'kg' );
               
                if( $weight > $weightLimit ) {
                        $message = sprintf( __( 'Sorry, %d kg exceeds the maximum weight of %d kg for %s', 'shiphero' ), $weight, $weightLimit, $ShipHero_Shipping_Method->title );
                            
                        $messageType = "error";
                        if( ! wc_has_notice( $message, $messageType ) ) {
                        
                            wc_add_notice( $message, $messageType );
                     
                        }
                }
            }       
        } 
    }
    add_action( 'woocommerce_review_order_before_cart_contents', 'shiphero_validate_order' , 10 );
    add_action( 'woocommerce_after_checkout_validation', 'shiphero_validate_order' , 10 );
}