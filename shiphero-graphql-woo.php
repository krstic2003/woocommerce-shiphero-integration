<?php
/*
Plugin Name:  Shiphero GraphQL Woo integration 
Plugin URI:   https://krstic.info/
Description:  Shiphero WooCommerce integration via GraphQL
Version:      1.0
Author:       Aleksandar Krstic
Author URI:   https://krstic.info/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  shiphero-graphql-woo
*/

// admin settings
require 'include/settings.php';

// add custom shipping method
// require 'include/shipping.php';

// add custom field to products
require 'include/product.php';

// order sync to shiphero
require 'include/order.php';


if( is_admin() ){
    

    $shiphero_settings_page = new ShipheroSettingsPage();

    if(isset( $_GET['page'] ) && $_GET['page'] == 'shiphero-setting-admin' ){
        echo '<div style="text-align:center;width:100%;position:absolute;top:50%;z-index:9999;"><a style="background-color:green;color:#ffffff;padding:10px 20px;" href="'.esc_url( admin_url( 'options-general.php?page=shiphero-setting-admin&cron_sh_up=1' ) ) .'">RUN Products Update from Shiphero</a><br><br><br></div>';
    }
}

add_action( 'init', 'process_sh_sync' );
function process_sh_sync() {
    if( isset( $_GET['cron_sh_up'] ) ) {
        $token = sh_login();
        $args = array(
            'status' => array( 'publish' ),
        );

        // Array of product objects
        $products = wc_get_products( $args );

       // $updated = [];

      //  $i = 0;

        // Loop through list of products
        foreach( $products as $product ) {

            // Collect product variables
            $product_sku = $product->get_sku();
            

            $sh_product = check_product($product_sku, $token);
           // write_log("---------START------------");
           // write_log($sh_product['data']['product']['data']);
           // write_log("---------END------------");



            if(! isset($sh_product['errors']) ){

                if(isset($sh_product['data']['product']['data']['price']) && $sh_product['data']['product']['data']['price'] !== null){
                    $new_price = $sh_product['data']['product']['data']['price'];
                }else{
                    $new_price = $product->get_regular_price();
                }

                if(isset($sh_product['data']['product']['data']['value']) && $sh_product['data']['product']['data']['value'] !== null){
                    $new_sell_price = $sh_product['data']['product']['data']['value'];
                }else{
                    $new_sell_price = $product->get_sale_price();
                }

                if(isset($sh_product['data']['product']['data']['warehouse_products'][0]['on_hand']) && $sh_product['data']['product']['data']['warehouse_products'][0]['on_hand'] !== null){
                    $new_qty = $sh_product['data']['product']['data']['warehouse_products'][0]['on_hand'];
                }else{
                    $new_qty = $product->get_stock_quantity();
                }

                if(isset($sh_product['data']['product']['data']['dimensions']) && $sh_product['data']['product']['data']['dimensions'] !== null){
                    $new_width = $sh_product['data']['product']['data']['dimensions']['width'];
                    $new_height = $sh_product['data']['product']['data']['dimensions']['height'];
                    $new_weight = $sh_product['data']['product']['data']['dimensions']['weight'];
                    $new_length = $sh_product['data']['product']['data']['dimensions']['length'];
                }else{
                    $new_width = $product->get_width();
                    $new_height = $product->get_height();
                    $new_weight = $product->get_weight();
                    $new_length = $product->get_length();
                }

                $product_update = wc_get_product( $product->get_id() );
                $product_update->set_price( $new_price );
                $product_update->set_sale_price( $new_sell_price );
                $product_update->set_manage_stock( true ); 
                $product_update->set_stock_quantity( $new_qty );
                $product_update->set_width( $new_width );
                $product_update->set_height( $new_height );
                $product_update->set_weight( $new_weight );
                $product_update->set_length( $new_length );
                $product_update->save();

                //$updated[$i] = $product_update;
               // $i++;
            }
        }



        //write_log($updated);
       // echo 'Done';
        //var_dump($updated);
    }
}